<?php
	/*
	Template Name: Trang thanh toán
	*/
?>
<?php get_header(); ?>
<?php $cart = new BIC_Cart; ?>
<section class="checkout fw">
    <div class="container">
        <div class="">
            <div class="cart-content">
                <nav class="breadcrumbs">
                    <?php if ( function_exists( 'yoast_breadcrumb' ) ) {
                        yoast_breadcrumb('','');};
                    ?>
                </nav>
                <div class="cart-detail">
                    <form action="" method="POST" id="CartConfirm">
                        <div class="info-customer">
                            <div class="formbox">
                                <div class="form-group">
                                    <label for="control-label">Họ tên <sup>*</sup></label>
                                    <input type="text" name="name" id="name" class="input-text" required requiredmsg="Vui lòng nhập tên của bạn !"/>
                                </div>
                                <div class="form-group pull-right">
                                    <label for="control-label">Điện thoại <sup>*</sup></label>
                                    <input type="tel" name="phone" id="phone" class="input-text" required requiredmsg="Vui lòng nhập số điện thoại của bạn !" />
                                </div>
                                <div class="form-group">
                                    <label for="control-label">Email <sup>*</sup></label>
                                    <input type="email" name="email" id="email" class="input-text" required requiredmsg="Vui lòng nhập đúng địa chỉ email !" />
                                </div>
                                <div class="form-group pull-right">
                                    <label for="control-label">Địa chỉ nhận hàng <sup>*</sup></label>
                                    <input type="text" name="address" id="address" class="input-text" required requiredmsg="Vui lòng nhập địa chỉ của bạn !" />
                                </div>
                                <div class="form-group">
                                    <label for="control-label">Ghi chú</label>
                                    <textarea cols="4" rows="5" class="input-text input-textarea" name="note" id="note"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="cart-product-list fw">
                            <h2><span class="cart-title">Chi tiết đơn hàng</span></h2>
                             <table class="table">
                                <thead>
                                    <tr>
                                        <th>Ảnh sản phẩm</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Giá</th>
                                        <th>Số lượng</th>
                                        <th>Thành tiền</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $cur = ' VNĐ';
                                    if($cart->total_items() > 0){
                                        //get cart items from session
                                        $cartItems = $cart->contents();
                                        foreach($cartItems as $item){
                                    ?>
                                    <tr>
                                        <td><figure><img src="<?php echo $item["image"]; ?>"></figure></td>
                                        <td><?php echo $item["name"]; ?></td>
                                        <td><?php echo bicweb_format_price($item["price"], $cur); ?></td>
                                        <td><?php echo $item["qty"]; ?></td>
                                        <td><?php echo bicweb_format_price($item["subtotal"], $cur); ?></td>
                                        <td></td>
                                    </tr>
                                    <?php } }else{ ?>
                                    <tr><td colspan="5"><p>Đang trống !</p></td>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td></td>
                                        <?php if($cart->total_items() > 0){ ?>
                                        <td></td>
                                        <td style="text-align: left; font-weight: bold;">Tổng</td>
                                        <td></td>
                                        <td><strong><?php echo bicweb_format_price($cart->total(), $cur); ?></strong></td>
                                        <td></td>
                                        <?php } ?>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="link-continue">
                            <?php
                                $cart = new BIC_Cart;
                            ?>
                            <button type="submit" href="javascript:void(0)" class="btn <?php echo ($cart->total_items() > 0) ? 'btn-success' : 'disabled' ?> pull-right">Gửi mail đặt hàng<i class="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                    <div class="ajaxLoad"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
    var elements = $("input, select");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                 e.target.setCustomValidity(e.target.getAttribute("requiredmsg"));
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
})
</script>
<script type="text/javascript">
    $("#CartConfirm").submit(function(e) {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            url: ajax_obj.ajax_url,
            data: {
                'action': 'BIC_cart_confirm',
                'messege': $('#note').val(),
                'name' : $('#name').val(),
                'phone': $('#phone').val(),
                'email' : $('#email').val(),
                'address' : $('#address').val(),
                nonce : ajax_obj.smart_nonce
            },
            beforeSend: function(){
                $('.ajaxLoad').show(300);
            },
            success: function(response){
                data = jQuery.parseJSON(response);
                $('.ajaxLoad').hide(300);
                $('#CartConfirm')[0].reset();
                $('.cart-detail').html('<span>' + data.msg + '</span><a href="/" class="btn btn-primary back-home"><?php _e('Quay về trang chủ', 'bicweb'); ?></a>');
            }
        });
        e.preventDefault();
    });
</script>
<?php get_footer(); ?>