<?php get_header(); ?>
<section class="product-detail fw">
	<div class="container">
		<div class="product-detail-content fw">
			<nav class="breadcrumbs">
				<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb('','');};
				?>
			</nav>
			<?php
				$terms = wp_get_object_terms($post->ID, 'san-pham-category');
    			if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
			?>
			<div class="product-detail-info">
				<!-- <div class="p-title"><?php echo $term->name;?></div> -->
				<div class="p-title"><h1><a><?php the_title();?></a></h1></div>
				<div class="share">
					<div class="news-detail-date">Ngày đăng: <?php echo get_the_date('Y-m-d');?></div>
					<?php setPostViews(get_the_ID()); ?>
					<div class="news-detail-view">Lượt xem: <?php echo getPostViews(get_the_ID()); ?></div>
					<?php get_template_part("inc/social-bar"); ?>
				</div>

				<div class="p-detail fw">
					<div class="p-img">
						<figure><a href="<?php echo bicweb_get_thumbnail_url('full') ?>" class="fancybox" rel="p-gallery"><img src="<?php echo bicweb_get_thumbnail_url('full') ?>" alt="<?php the_title();?>" /></a></figure>
						<div class="p-thumbs">
						  	<ul>
								<?php
								   $str = do_shortcode('[types field="thu-vien-anh" url="true" separator=","][/types]');
								   $str_small = do_shortcode('[types field="thu-vien-anh" url="true" separator="," size="thumbnail"][/types]');
								   $images = explode(",", $str);
								   $images_small = explode(",", $str_small);
								   $count_img = count($images);
								   if($count_img > 0){
									   for($x = 0; $x < $count_img; $x++){
									    	if($images[$x]!="")
									   		echo '<li><a class="fancybox" rel="p-gallery" href="'.$images[$x].'"><img src="'.$images_small[$x].'" alt="'.get_the_title().'" /></a></li>';
									   }
								   }
							  	?>
						  	</ul>
						</div>
					</div>
					<div class="p-detail-info">
						<div class="p-detail-form">
							<div>
								<span>Mã sản phẩm</span> <span>:</span>
								<span><?php echo types_render_field( "ma-san-pham", array("output"=>"raw") );?></span>
							</div>
							<div>
								<span>Xuất xứ</span>     <span>:</span>
								<span><?php echo types_render_field( "xuat-xu-sp", array("output"=>"raw") );?></span>
							</div>
							<div>
								<span>Chất lượng</span>  <span>:</span>
								<span><?php echo types_render_field( "chat-luong-sp", array("output"=>"raw") );?></span>
							</div>
							<div>
								<span>Cảnh vẽ</span>     <span>:</span>
								<span><?php echo types_render_field( "canh-ve-sp", array("output"=>"raw") );?></span>
							</div>
							<div>
								<span>Mầu vẽ</span>      <span>:</span>
								<span><?php echo types_render_field( "mau-ve-sp", array("output"=>"raw") );?></span>
							</div>
							<div>
								<span>Loại men</span>    <span>:</span>
								<span><?php echo types_render_field( "loai-men-sp", array("output"=>"raw") );?></span>
							</div>
						</div>
						<div class="vat">
							<?php echo types_render_field( "thong-tin-ship-hang", array("output"=>"raw") );?>
						</div>
						<div class="season">
							<div class="p-price">
								<?php
									$oldprice = types_render_field( "gia-cu", array("output"=>"raw") );
									$price = types_render_field( "gia-moi", array("output"=>"raw") );
									echo bicweb_get_price($oldprice, $price);
								?>
							</div>
							<?php
								if($oldprice != 0){
									echo '<a class="add-to-cart" href="javascript:void(0)" data-id="'.get_the_ID().'"><span>Chọn mua</span></a>';
								}
							?>
						</div>
					</div>
				</div>
				<div class="p-info fw">
					<div class="p-excerpt"><?php the_excerpt();?></div>
					<?php the_content();?>
				</div>
			</div>

			<?php get_template_part("inc/related-product"); ?>
		</div>
		<?php get_sidebar("news");?>
	</div>
</section>
<script type="text/javascript">
	$('.add-to-cart').on('click', function(){
		var id = $(this).data("id");
		addToCart(id);
	});
	$(document).ready(function(){
    	if($('.fancybox').length){
    		$('.fancybox').fancybox();
    	}
	});
</script>
<?php get_footer(); ?>