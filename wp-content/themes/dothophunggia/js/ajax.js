$(document).ready(function(){
	$(document).on('click','#reset_btn',function(){
		count = 0;
		email = $('#femail').val();
		if(!IsEmail( email )){
			$('#femail-error').html('Email không hợp lệ');
			count++;
		}else $('#femail-error').html('');
		
		if( count == 0){
			forget_password();
		}else{
			return false;
		}		
	});
	
	$(document).on('click','#login_btn',function(){
		count = 0;
		email = $.trim( $('#login_username').val() );
		if(email == '' ){ 
			$('#email-error').html('Vui lòng nhập tên đăng nhập'); count++; 
		}else{
			$('#email-error').html('');
		}
		password = $.trim( $('#password').val() );
		if(password == '' ){ 
			$('#password-error').html('Vui lòng nhập mật khẩu'); count++; 
		}else{
			if(password.length < 4 ){ 
				$('#password-error').html('Mật khẩu không hợp lệ'); count++;  
			}else{
				$('#password-error').html('');
			}
		}
		if( count == 0){
			login();
		}else{
			return false;
		}		
	});

	$(document).on('click','#register_btn',function(){
		count = 0;
		error = $('#username-error').data('error');
		if( error == 1 )count++;
		email = $('#remail').val();
		if(!IsEmail( email )){
			$('#remail-error').html('Email không hợp lệ');
			count++;
		}else $('#remail-error').html('');

		if( count == 0){
			register();
		}else{
			return false;
		}
		
	});
	
	$(function(){
		  $(".entry-title a").each(function(i){
		    len=$(this).text().length;
		    if(len>90)
		    {
		      $(this).text($(this).text().substr(0,90)+'...');
		    }
		  });
	});
});

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}	

$(document).on('blur','#username',function(){
	if($(this).val()==null || $(this).val()==''){
		$('#username-error').html('Bạn chưa nhập tên đăng nhập.');
	}
	else{
		jQuery.post(
			ajax_obj.ajax_url, 
		    {
		        'action': 'user_exists',
		        'username':   $(this).val(),
		        nonce : ajax_obj.smart_nonce
		    }, 
		    function(response){
		    	data = jQuery.parseJSON(response);
		        if( data.flag == false ){
		        	$('#username-error').html(data.msg);
		        	$('#username-error').data('error', '1');
			    }else{
			    	$('#username-error').html('');
			    	$('#username-error').data('error', '0');
				}
		    }
		);
	}
});

$(document).on('blur','#remail',function(){	
	if(!IsEmail($(this).val())){
		$('#remail-error').html('Email không hợp lệ.');
	}
	else{
		$('#register_btn').button('loading');
		jQuery.post(
			ajax_obj.ajax_url, 
		    {
		        'action': 'email_exists',
		        'email':   $(this).val(),
		        nonce : ajax_obj.smart_nonce
		    }, 
		    function(response){
		    	$('#register_btn').button('reset');
		    	data = jQuery.parseJSON(response);
		        if( data.flag == false ){
		        	$('#remail-error').html(data.msg);
		        	$('#remail-error').data('error', '1');
			    }else{
			    	$('#remail-error').html('');
			    	$('#remail-error').data('error', '0');
				}
		    }
		);
	}
});

function login(){
	$('#login_btn').button('loading');
	jQuery.post(
		ajax_obj.ajax_url, 
	    {
	        'action': 'login',
	        'data':   'foobarid',
	        'username' : $('#login_username').val(),
	        'password' : $('#password').val(),
	        nonce : ajax_obj.smart_nonce
	    }, 
	    function(response){
	        data = jQuery.parseJSON(response);
	        if( data.flag == true ){
				location.reload();
		    }else{
		    	$('#login_btn').button('reset');
				$('#login_fail').show();
			}
	    }
	);
}

function forget_password(){
	$('#reset_btn').button('loading');
	jQuery.post(
		ajax_obj.ajax_url, 
	    {
	        'action': 'ureset_password',
	        'email' : $('#femail').val(),
	        nonce : ajax_obj.smart_nonce
	    }, 
	    function(response){
	    	$('#reset_btn').button('reset');
	        data = jQuery.parseJSON(response);
	        if( data.flag == true ){
	        	$('#reset_fail').show();
	        	$('#reset_fail').removeClass('response_error').addClass('response_success');
				$('#reset_fail').html(data.msg);
		    }else{
				$('#reset_fail').show();
				$('#reset_fail').removeClass('response_success').addClass('response_error');
				$('#reset_fail').html(data.msg);
			}
	    }
	);
}

function register(){
	$('#register_btn').button('loading');
	jQuery.post(
		ajax_obj.ajax_url, 
	    {
	        'action': 'uregister',
	        'username' : $('#username').val(),
	        'email' : $('#remail').val(),
	        nonce : ajax_obj.smart_nonce
	    }, 
	    function(response){
	    	$('#register_btn').button('reset');
	        data = jQuery.parseJSON(response);
	        if( data.flag == true ){
	        	$('#registration_fail').show();
	        	$('#registration_fail').removeClass('response_error').addClass('response_success');
				$('#registration_fail').html(data.msg);
				$('#registration-span').hide();
		    }else{
				$('#registration_fail').show();
				$('#registration_fail').removeClass('response_success').addClass('response_error');
				$('#registration_fail').html(data.msg);
			}
	    }
	);
}

function addToCart(id){
	jQuery.post(
		ajax_obj.ajax_url, 
	    {
	        'action': 'BIC_cart_action',
	        'act' : 'addToCart',
	        'pid' : id,
	        nonce : ajax_obj.smart_nonce
	    }, 
	    function(response){
	        data = jQuery.parseJSON(response);
	        if( data.flag == true ){
	        	alert(data.msg);
		    }else{
				alert(data.msg);
			}
	    }
	);
}
// function searchaddToCart(id){
// 	jQuery.post(
// 		ajax_obj.ajax_url, 
// 	    {
// 	        'action': 'searchaddToCart',
// 	        'act' : 'searchaddToCart',
// 	        'sid' : id,
// 	        nonce : ajax_obj.smart_nonce
// 	    }, 
// 	    function(response){
// 	        data = jQuery.parseJSON(response);
// 	        if( data.flag == true ){
// 	        	alert(data.msg);
// 		    }else{
// 				alert(data.msg);
// 			}
// 			alert(response);
// 	    }
// 	);
// }
function updateCart(id,qty){
	jQuery.post(
		ajax_obj.ajax_url, 
	    {
	        'action': 'BIC_cart_action',
	        'act' : 'updateItem',
	        'pid' : id,
	        'qty' : qty,
	        nonce : ajax_obj.smart_nonce
	    }, 
	    function(response){
	        data = jQuery.parseJSON(response);
	        if( data.flag == true ){
	        	// alert(data.msg);
	        	location.reload();
		    }else{
				alert(data.msg);
			}
	    }
	);
}
function deleteCart(id){
	if(confirm("Bạn có chắc không ???")){
		if(id != 0 || id != ""){
	        jQuery.ajax({
	            type: "POST",
	            data: {
	                'action': 'BIC_cart_action',
	                'act' : 'removeItem',
	                'pid' : id,
	                nonce : ajax_obj.smart_nonce
	            },
	            url: ajax_obj.ajax_url,        
	            beforeSend:function(){
	                $('.ajaxLoad').show(1);
	            },
	            success:function(response){
	                data = jQuery.parseJSON(response);
		        if( data.flag == true ){
		        	location.reload();
			    }else{
					alert(data.msg);
				}
	                $('.ajaxLoad').hide();
	            }
	        });
	    }
	}
}