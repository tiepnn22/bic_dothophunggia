function news_highlight() {
    if ($('.news-highlight .n-group').length > 1) {
        $('.news-highlight .n-items').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            slideSpeed: 2000,
            smartSpeed: 2000,
            nav: true,
            dots: false,
            autoWidth: false,
            items: 4,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                320:{
                    items:2,
                    nav:true
                },
                600:{
                    items:3,
                    nav:true
                },
                900:{
                    items:4,
                    nav:true
                },
                1220:{
                    items:4,
                    nav:true
                }
            }
        });
    }
}
function news_related() {
    if ($('.news-related .n-group').length > 1) {
        $('.news-related .n-items').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            slideSpeed: 2000,
            smartSpeed: 2000,
            nav: true,
            dots: false,
            autoWidth: false,
            items: 2,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:true
                }
            }
        });
    }
}
// function sidebar_product_highlight() {
//     if ($('.sidebar-product-highlight .p-group').length > 1) {
//         $('.sidebar-product-highlight .p-items').owlCarousel({
//             loop: true,
//             autoplay: true,
//             margin: 20,
//             slideSpeed: 2000,
//             smartSpeed: 2000,
//             nav: true,
//             dots: false,
//             autoWidth: false,
//             items: 1,
//             navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
//         });
//     }
// }
function product_related() {
    if ($('.product-related article').length > 1) {
        $('.product-related .p-group').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            slideSpeed: 2000,
            smartSpeed: 2000,
            nav: true,
            dots: false,
            autoWidth: false,
            items: 3,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                440:{
                    items:2,
                    nav:true
                },
                655:{
                    items:3,
                    nav:true
                },
                767:{
                    items:2,
                    nav:true
                },
                992:{
                    items:3,
                    nav:true
                },
                1220:{
                    items:3,
                    nav:true
                }
            }
        });
    }
}
function banner2() {
    if ($('.banner2 .banner-img').length > 1) {
        $('.banner2 .content-banner').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 0,
            slideSpeed: 2000,
            smartSpeed: 2000,
            nav: true,
            dots: false,
            autoWidth: false,
            items: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        });
    }
}
function product_list() {
    // if ( ($('.product-list article').length == 1) ) { } else {
        if ( ($('.product-list article').length > 1) ) {
            $('.product-list .p-group').owlCarousel({
                loop: true,
                autoplay: false,
                margin: 20,
                slideSpeed: 2000,
                smartSpeed: 2000,
                nav: false,
                dots: false,
                autoWidth: false,
                items: 5,
                navText: ['', ''],
                responsive:{
                    0:{
                        items:2,
                        nav:true
                    },
                    440:{
                        items:2,
                        nav:true
                    },
                    700:{
                        items:3,
                        nav:true
                    },
                    992:{
                        items:4,
                        nav:true
                    },
                    1220:{
                        items:5,
                        nav:true
                    }
                }
            });
        }
    // }
}
function p_thumbs() {
    if ($('.p-thumbs img').length >  1) {
        $('.p-thumbs ul').owlCarousel({
            loop: true,
            autoplay: true,
            margin: 20,
            slideSpeed: 2000,
            smartSpeed: 2000,
            nav: true,
            dots: true,
            autoWidth: false,
            items: 3,
            navText: ['', ''],
        });
    }
}
$(document).ready(function(){
    $('.menu li').hover(function(){
        $(this).find('ul:first').stop(true,false).fadeToggle();
    });
    if ($('.main-menu').length) {
        $('.main-menu').meanmenu({
            meanScreenWidth: "1100",
            meanMenuContainer: ".mobile-menu",
        });
    }
    if($('.venobox').length){
        $('.venobox').venobox();
    }
    $('.popup-plush').on('click', function (){
        $('.popup-content').stop(true,false).slideToggle();
    });
    $('.fix-search-mobile').on('click', function (){
        $('.search-box').stop(true,false).slideToggle();
    });

    news_highlight();
    news_related();
    // sidebar_product_highlight();
    product_related();
    banner2();
    product_list();
    p_thumbs();

    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 50) {
            $('header').addClass('stick');
            $('.header-top').css("display", "none");
            $('.logo img').css("max-width", "100px");
            // $('.logo').css("left", "0");
            $("#back-to-top").fadeIn(300);
        } else {
            $('header').removeClass('stick');
            $('.header-top').css("display", "block");
            $('.logo img').css("max-width", "100%");
            // $('.logo').css("left", "14%");
            $("#back-to-top").fadeOut(300);
        }
    });
    if($('#back-to-top').length){
        $("#back-to-top").on('click', function() {
           $('html, body').animate({
               scrollTop: $('html, body').offset().top
             }, 1000);
        });
    }
});