<?php get_header(); ?>
	<?php
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$term_id = $term->term_id;
		$taxonomy_name = 'san-pham-category';

		$term_childs = get_term_children( $term_id, $taxonomy_name );

		$count = count($term_childs);
		if($count <= 0) {
			get_template_part("inc/product-list-detail");
		} else {
			get_template_part("inc/product-list");
		}
	?>
<?php get_footer(); ?>