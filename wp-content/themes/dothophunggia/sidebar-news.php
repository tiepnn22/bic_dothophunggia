<div class="sidebar fw">
	<aside class="sidebar-news-highlight fw">
		<div class="title">
			<h2><a>Tin tức nổi bật</a></h2>
		</div>
		<div class="n-items fw">
			<div class="n-group">


<?php $query = new WP_Query(array('cat'=>6,'showposts'=>10,'order' => 'DESC','orderby' => 'date'));
if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();?>
	<article>
		<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('p-detail') ?>" alt="<?php the_title();?>" /></a></figure>
		<div class="n-title"><h3><a href="<?php the_permalink();?>"><?php echo cut_string(get_the_title(),80,'...'); ?></a></h3></div>
		<div class="n-date"><?php echo get_the_date('Y-m-d');?></div>
	</article>
<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>


			</div>
		</div>
	</aside>
</div>