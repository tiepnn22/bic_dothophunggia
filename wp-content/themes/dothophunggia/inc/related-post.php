<aside class="news-related fw">
	<div class="news-related-title">
		<h2><a>Tin liên quan</a></h2>
	</div>
	<div class="n-items fw">
		<div class="n-group">
	<?php
	    global $post;
		$orig_post = $post;
	    $categories = get_the_category($post->ID);
		    if ($categories) {
			    $category_ids = array();
			    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			    $args=array(
				    'category__in' => $category_ids,
				    'post__not_in' => array($post->ID),
				    'posts_per_page'=> 1000,
				    // 'caller_get_posts'=>1
				    'ignore_sticky_posts'=>1
			    );
			    $my_query = new wp_query( $args ); $i=0;
			    if( $my_query->have_posts() ) {
				    // echo '<ul>';
				    while( $my_query->have_posts() ) {
					    $my_query->the_post();

							if($i!=0&&$i%3==0) {echo '</div><div class="n-group">';} ?>

							<article>
								<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('thumbnail') ?>" alt="<?php the_title();?>" /></a></figure>
								<div class="n-title"><h3><a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php echo cut_string(get_the_title(),80,'...'); ?></a></h3></div>
								<div class="n-date">(<?php echo get_the_date('Y-m-d'); ?>)</div>
							</article>

					    <?php  $i++;
				    }
				    // echo '</ul>';
			    } else { echo '<div class="update-loading">Đang cập nhật!</div>'; }
		    }
	    $post = $orig_post;
	    wp_reset_query(); 
	?>
		</div>
	</div>
</aside>
