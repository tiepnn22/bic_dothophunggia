<?php
	global $post;
	$terms = get_the_terms( $post->ID , 'san-pham-category', 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
			'post_type' => 'san-pham',
			'tax_query' => array(
								array(
										'taxonomy' => 'san-pham-category',
										'field' => 'id',
										'terms' => $term_ids,
										'operator'=> 'IN'
								 )),
			'posts_per_page' => 1000,
			'orderby' => 'date',
			'post__not_in'=>array($post->ID)
	 ) );
?>
<div class="product-related fw">
	<div class="title">
		<h2><a>Sản phẩm cùng loại</a></h2>
	</div>
	<div class="p-items fw">
		<div class="p-group">
		<?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
			<article>
				<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('p-detail') ?>" alt="<?php the_title();?>" /></a></figure>
				<div class="p-title">
					<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
				</div>
				<div class="p-price">
					<?php
				$oldprice = types_render_field( "gia-cu", array("output"=>"raw") );
				$price = types_render_field( "gia-moi", array("output"=>"raw") );
				if($oldprice == 0){

				} else {
					if($price == 0){
						// echo '<span class="no-sale" href="javascript:void(0)"></span>';
					} else {
						// echo '<span class="sale" href="javascript:void(0)"></span>';
						$a = (1 - ($price / $oldprice))*100;
						// echo ceil($a)."%";
						echo '<span class="sale-number">-'.ceil($a).'%</span>';
					}
				}
				echo bicweb_get_price($oldprice, $price);
			?>
				</div>
			</article>
		<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>
		</div>
	</div>
</div>