<?php get_header(); ?>
<section class="product-list fw">
	<div class="container">
	<?php
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$term_id = $term->term_id;
		$taxonomy_name = 'san-pham-category';

		$term_childs = get_term_children( $term_id, $taxonomy_name );

		$count = count($term_childs);
		if($count<=0) {
			echo '<span class="empty">'.__("Đang cập nhật...","bicweb").'</span>';
		} else {
			echo '<ul>';
			    foreach ( $term_childs as $child ) {
			        $term = get_term_by( 'id', $child, $taxonomy_name );
					echo '<li>'; ?>

						<div class="product-list-content fw">
							<div class="title">
								<h2><a href="<?php echo get_term_link( $term->term_id, $taxonomy_name );?>"><?php echo $term->name; ?></a></h2>
								<a href="<?php echo get_term_link( $term->term_id, $taxonomy_name );?>">Xem thêm</a>
							</div>
							<div class="p-items fw">
								<div class="p-group">


<?php $query = gda_custom_posttype_query('san-pham', 'san-pham-category', $term->term_id, 1000);
if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>
	<article>
		<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('p-detail') ?>" alt="<?php the_title();?>" /></a></figure>
		<div class="p-title"><h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3></div>
		<div class="p-price">
			<?php
				$oldprice = types_render_field( "gia-cu", array("output"=>"raw") );
				$price = types_render_field( "gia-moi", array("output"=>"raw") );
				if($oldprice == 0){

				} else {
					if($price == 0){
						// echo '<span class="no-sale" href="javascript:void(0)"></span>';
					} else {
						// echo '<span class="sale" href="javascript:void(0)"></span>';
						$a = (1 - ($price / $oldprice))*100;
						// echo ceil($a)."%";
						echo '<span class="sale-number">-'.ceil($a).'%</span>';
					}
				}
				echo bicweb_get_price($oldprice, $price);
			?>
		</div>
	</article>
<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>


								</div>
							</div>
						</div>
				<?php
					echo '</li>';
				}
			echo '</ul>';
		}
	?>
	</div>
</section>
<?php get_footer(); ?>