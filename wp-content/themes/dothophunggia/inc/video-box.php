<div class="video-content video-home">
    <div class="video-play" id="videoPlayer"></div>
    <div class="video-list">
        <?php
            // $term_ids = 20;
            $queryvideo = new WP_Query( array(
                    'post_type' => 'video',
                    // 'tax_query' => array(
                    //                     array(
                    //                             'taxonomy' => 'video-categories',
                    //                             'field' => 'id',
                    //                             'terms' => $term_ids,
                    //                             'operator'=> 'IN'
                    //                      )),
                    'showposts'=>3,
                    'order' => 'DESC',
                    'orderby' => 'date'
             ) );
            $stt = 0;
            $videoUrl = "";
            //$videoImage = "";
            $videoTitle = "";
        ?>
        <ul>
        <?php if($queryvideo->have_posts()) : while($queryvideo->have_posts()) : $queryvideo->the_post(); ?>
            <?php
                if($stt == 0):
                    $videoUrl = get_the_excerpt(); //types_render_field( "link-youtube", array("output"=>"raw") );
                    //$videoImage = the_post_thumbnail_url("thumb");
                    $videoTitle = the_title('','',false);
            ?>
                <input type="hidden" name="videoImage" id="videoImg" value="<?php the_post_thumbnail_url("medium"); ?>">
            <?php endif; ?>
            <li>
                <a href="javascript:void(0)" onclick="loadvideo('<?php echo get_the_excerpt(); //types_render_field( "link-youtube", array("output"=>"raw") ); ?>','<?php the_title();?>','<?php the_post_thumbnail_url("medium"); ?>')">

                    <!-- <?php the_title();?> -->
                    <?php echo __( the_title(), 'bicweb' ); ?>
                </a>
            </li>
        <?php $stt++; endwhile;  wp_reset_query(); endif; ?>
        </ul>
        <script type="text/javascript">
            jwplayer.key = "YF5WouVCtlsIhBT9mSc5oWNjigOSlwz8ewUnTg==";
            var playerInstance = jwplayer('videoPlayer').setup({
                flashplayer: '<?php bloginfo('template_url');?>/assets/jwplayer7/jwplayer.flash.swf',
                controlbar: 'bottom',
                aspectratio: '4:3'
            });
            playerInstance.setup({
                playlist: [{
                      file: '<?php echo $videoUrl ?>',
                      image: $("#videoImg").val(),
                      title: '<?php echo __( $videoTitle, 'bicweb' ) ?>'
                }],
                width: '100%',
                height: '195',
                stretching:'exactfit',
                logo: {
                    file: '',
                    link: '',
                    position: 'top-left' //top-right (the default), top-left, bottom-right or bottom-left
                }
                });
            function loadvideo(vFile,vTitle,vImage) {
                jwplayer().load([{
                    file: vFile,
                    image: vImage,
                    title: vTitle
                }]);
                //jwplayer().play();
                playerInstance.play();
            };
        </script>
    </div>
</div>
<style>
    .video-content iframe {
        position: relative !important;
    }
</style>