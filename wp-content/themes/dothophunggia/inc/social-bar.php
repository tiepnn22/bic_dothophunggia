<div class="social-bar" style="float: right;">
    <div class="print" style="display:inline-block; font-size:18px; color: #000;">
        <button style="border:none; background:#ffffff" onclick="window.print()">
            <i class="fa fa-print" aria-hidden="true"></i>
        </button>
    </div>
    <!-- <div class="sharemail" style="display:inline-block; font-size:18px; color: #000;">
        <a style="display:inline-block; font-size:18px; color: #000;" href="mailto:?subject=<?php echo sprintf( __( "Chia sẻ từ %s", "bicweb" ), get_option( 'blogname' )); ?>&amp;body=<?php the_permalink();?>" title="Share by Email">
            <span class="fa fa-envelope-o"></span>
        </a>
    </div> -->
    <div class="btgg" style="margin-right:0px;display:inline-flex">
        <div class="g-plusone" data-count="false" data-size="medium" data-href='<?php the_permalink();?>'></div>
    </div>
    <div class="btfbl" style="position:relative;bottom:10px;margin-right:0px;display:inline-flex;position:relative;top:-5px;">
        <div class="fb-like" data-href="<?php the_permalink();?>" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
    </div>
    <!-- <div class="btfbs">
        <div class="fb-send" data-href="<?php the_permalink();?>">
    </div> -->
    <div style="clear: both;"></div>
</div>


<script type="text/javascript">
    window.___gcfg = { lang: 'vi' };
    (function () {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>