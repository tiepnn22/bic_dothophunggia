<?php
//tắt tất cả update
    function remove_core_updates(){
    global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','remove_core_updates');
    add_filter('pre_site_transient_update_plugins','remove_core_updates');
    add_filter('pre_site_transient_update_themes','remove_core_updates');
// Includes
include_once get_template_directory(). '/func/init.php';
if ( ! function_exists( 'bicweb_setup' ) ) :
	function bicweb_setup() {
		//Thêm chức năng set thumbnails image cho tưng bài post, page
		// add_theme_support( 'post-thumbnails' );
		// set_post_thumbnail_size( 120, 75 ); //Set default thumbnail size

		//Đăng ký menu
		register_nav_menus( array(
			'primary' => __( 'Menu chính', 'bicweb' ),
			'secondary'  => __( 'Menu Chân trang', 'bicweb' ),
			'product'  => __( 'Menu sản phẩm', 'bicweb' ),
		) );
		//Cho phép người dùng sử dụng HTML5 trong danh sách bình luận, form comment, search form, galleries.
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		//thêm các định dạng cho bài post
		add_theme_support( 'post-formats', array(
			'aside',	//Tương tự như ghi chú nhanh. Được dùng cho những post dạng như cập nhật nhanh hoặc status
			'image',	//Một post chứa dạng thư viện hình ảnh.
			'video',	//Post có thể chứa một hoặc vài video. Trong WordPress codex có hướng dẫn cách dùng 1 url của video trên theme thông qua player sẵn có của WordPress mà không cần dùng đến các tính năng nhúng (embed) của các video.
			'quote',	//Một đoạn văn hay đoạn trích dẫn.
			'link',		//Một link đơn giản đến site bên ngoài.
			'gallery',	//Một post chứa dạng thư viện hình ảnh.
			'status',	//Tương tự như Aside, nhưng có thể nhiều hơn một dòng status đơn giản.
			'audio',	//Một file âm thanh hoặc list nhạc.
			'chat',		//Một bản sao chép lại của đoạn hội thoại
		) );
	}
endif;

add_action( 'after_setup_theme', 'bicweb_setup' );

//Đăng ký widget sidebar
function bicweb_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'bicweb' ),
		'id'            => 'sidebar',
		'description'   => __( 'Thêm widget vào cột trái', 'bicweb' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Quảng cáo giữa trang chủ', 'bicweb' ),
		'id'            => 'adv-top',
		'description'   => __( 'Chèn ảnh vào vùng quảng cáo giữa trang chủ (1170x250)', 'bicweb' ),
		'before_widget' => '<div id="%1$s" class="adv-home %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => __( 'Quảng cáo Chân trang', 'bicweb' ),
		'id'            => 'adv-bot',
		'description'   => __( 'Chèn quảng cáo vào chân trang (2 vùng ảnh)', 'bicweb' ),
		'before_widget' => '<div id="%1$s" class="adv-bottom %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'bicweb_widgets_init' );

/*-----------------------------------------------------------------------------------*/
/*  Post Thumbnail Support
/*-----------------------------------------------------------------------------------*/
if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'p-thumb', 280, 182, true );
    add_image_size( 'p-detail', 195, 130, true );
}

function bicweb_get_thumbnail_url( $size = 'full' ) {
    global $post;
    if (has_post_thumbnail( $post->ID ) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), $size );
        return $image[0];
    }
    // use first attached image
    $images =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
    if (!empty($images)) {
        $image = reset($images);
        $image_data = wp_get_attachment_image_src( $image->ID, $size );
        return $image_data[0];
    }  
    // use no preview fallback
    if ( file_exists( get_template_directory().'/images/nothumb-'.$size.'.png' ) )
        return get_template_directory_uri().'/images/nothumb-'.$size.'.png';
    else
        return get_template_directory_uri().'/images/logo.png';
}

if( !function_exists('get_post_thumb') ) {
	function get_post_thumb(){
		global $post ;
		if ( has_post_thumbnail($post->ID) ){
			$image_id = get_post_thumbnail_id($post->ID);  
			$image_url = wp_get_attachment_image_src($image_id,'full');  
			$image_url = $image_url[0];
			return $image_url;
		}
	}
}
function cut_string($str,$len,$more){
	if ($str=="" || $str==NULL) return $str;
	if (is_array($str)) return $str;
		$str = trim(strip_tags($str));
	if (strlen($str) <= $len) return $str;
		$str = substr($str,0,$len);
	if ($str != "") {
		if (!substr_count($str," ")) {
		  if ($more) $str .= " ...";
		  return $str;
		}
		while(strlen($str) && ($str[strlen($str)-1] != " ")) {
			$str = substr($str,0,-1);
		}
		$str = substr($str,0,-1);
		if ($more) $str .= " ...";
	}
	return $str;
}
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
function filter_search($query) {
    if ($query->is_search) {
 		$query->set('post_type', array('san-pham', 'post'));
    };
    return $query;
};
add_filter('pre_get_posts', 'filter_search');

// function bicweb_format_cost($money){
// 	$str = "";
// 	if($money != 0){
// 		$num = (float)$money;
// 		$str = number_format($num,0,'.',',');
// 		$str .= " VNĐ";
// 		$str = '<div class="p-price-new"><span>'.$str.'</span></div>';
// 	}
// 	else{
// 		$str = "Liên hệ: <a href='callto:'><span></span></a>";
// 	}
// 	return $str;
// }
// function bicweb_format_price($money, $cur){
// 	$str = "";
// 	if($money != 0){
// 		$num = (float)$money;
// 		$str = number_format($num,0,'.',',');
// 		$str .= $cur;
// 		$str = '<span>'.$str.'</span>';
// 	}
// 	return $str;
// }
// function bicweb_get_price($old_price, $price){
// 	$str = "";
// 	if($old_price > 0){
// 		if($price > 0 || $price != null){
// 			$str1 = " ".bicweb_format_price($price);
// 			$str2 = " ".bicweb_format_price($old_price);
// 			$str = '<div class="p-price-old">'.$str2.' VNĐ</div><div class="p-price-new"><span>'.$str1.'</span> VNĐ</div>';
// 		}
// 		else
// 		{
// 			$str = "".bicweb_format_price($old_price);
// 		}
// 	}
// 	else{
// 		$str = "Liên hệ";
// 	}
// 	return $str;
// }

function bicweb_format_price($money, $cur){
	$str = "";
	if($money != 0){
		$num = (float)$money;
		$str = number_format($num,0,'.',',');
		$str .= $cur;
		$str = '<span>'.$str.'</span>';
	}
	return $str;
}
function bicweb_get_price($old_price, $price){
	$donvi = " VNĐ";
	$str = "";
	if($old_price > 0){
		if($price > 0 || $price != null){
			$str1 = bicweb_format_price($price, $donvi);
			$str2 = bicweb_format_price($old_price, $donvi);
			$str = '<div class="price-old">'.$str2.'</div><div class="price-news">'.$str1.'</div>';
		} else {
			$str = bicweb_format_price($old_price, $donvi);
		}
	} else{
		$str = '<span><a class="contactsky" href="'.get_permalink(12).'">Liên hệ</a></span>';
	}
	return $str;
}
function bicweb_get_price_number($old_price, $price){
	if($price == 0 || $price == ""){
		return $old_price;
	} else {
		return $price;
	}
}

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
/*-----------------------------------------------------------------------------------*/
/* removes the WordPress version from your header for security
/*-----------------------------------------------------------------------------------*/
function bicweb_remove_wpversion() {
    return '<!--Theme by bicweb.vn-->';
}
add_filter( 'the_generator', 'bicweb_remove_wpversion' );

/*-----------------------------------------------------------------------------------*/
/* Remove class and ID from wp_nav_menu()
/*-----------------------------------------------------------------------------------*/
function wp_nav_menu_attributes_filter($var) {
	return is_array($var) ? array_intersect($var, array('current-menu-item')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
/*-----------------------------------------------------------------------------------*/
/* Custom Read More
/*-----------------------------------------------------------------------------------*/
function bicweb_readmore() {
	return __(' ...', 'bicweb');
}
add_filter('excerpt_more', 'bicweb_readmore');

/*=====Nhúng file style.css=====*/
function bicweb_style() {
	wp_register_style( 'bootstrap-style', get_template_directory_uri() . "/css/bootstrap.min.css",false, 'all' );
	wp_enqueue_style('bootstrap-style');
	wp_register_style( 'mainmenu-style', get_template_directory_uri() . "/css/meanmenu.css",false, 'all' );
	wp_enqueue_style('mainmenu-style');
	wp_register_style( 'owl-style', get_template_directory_uri() . "/css/owl.carousel.min.css",false, 'all' );
	wp_enqueue_style('owl-style');
	wp_register_style( 'main-style', get_template_directory_uri() . "/style.css",false, 'all' );
	wp_enqueue_style('main-style');
	// wp_register_style( 'responsive-style', get_template_directory_uri() . "/css/responsive.css",false, 'all' );
	// wp_enqueue_style('responsive-style');
	//JQuery
	wp_deregister_script('jquery');

	// Custom script
	//wp_enqueue_script ( string $handle, string $src = false, array $deps = array(), string|bool|null $ver = false, bool $in_footer = false )
	wp_register_script( 'jquery', get_template_directory_uri() . "/js/jquery-2.2.0.min.js",false,"2.2.0" );
	wp_enqueue_script('jquery');
	wp_register_script( 'bootstrap-script', get_template_directory_uri() . "/js/bootstrap.min.js", array('jquery'),false,true );
	wp_enqueue_script('bootstrap-script');
	wp_register_script( 'meanmenu', get_template_directory_uri() . "/js/jquery.meanmenu.min.js", array('jquery'),false,true );
	wp_enqueue_script('meanmenu');
	wp_register_script( 'owl-script', get_template_directory_uri() . "/js/owl.carousel.min.js", array('jquery'),false,true );
	wp_enqueue_script('owl-script');
	// wp_register_script( 'jwplayer', get_template_directory_uri() . "/assets/jwplayer7/jwplayer.js", array('jquery'),false,true );
	// wp_enqueue_script('jwplayer');

	wp_register_style( 'venobox-css', get_template_directory_uri() . "/assets/venobox/venobox.min.css",false, 'all' );
	wp_enqueue_style('venobox-css');
	wp_register_script('venobox-js', get_template_directory_uri() . "/assets/venobox/venobox.min.js", array('jquery'),false,true );
	wp_enqueue_script('venobox-js');

	if(is_page('dung-do-tho')){
		wp_register_script( 'select2', get_template_directory_uri() . "/js/select2.js", array('jquery'),false,true );
		wp_enqueue_script('select2');
	}
	if ( is_home() ) {
		wp_register_style( 'aos-style', get_template_directory_uri() . "/assets/aos/aos.css",false, 'all' );
		wp_enqueue_style('aos-style');
		wp_register_script( 'aos', get_template_directory_uri() . "/assets/aos/aos.js", array('jquery'),false,true );
		wp_enqueue_script('aos');
	}
	if(is_singular('san-pham')){
		wp_register_style( 'fancybox-css', get_template_directory_uri() . "/assets/fancybox/jquery.fancybox.css",false, 'all' );
		wp_enqueue_style('fancybox-css');
		wp_register_script( 'fancybox-js', get_template_directory_uri() . "/assets/fancybox/jquery.fancybox.pack.js", array('jquery'),false );
		wp_enqueue_script('fancybox-js');
	}
	if(is_single()){

	}
	wp_register_script( 'script', get_template_directory_uri() . "/js/main.js", array('jquery'),false,true );
	wp_enqueue_script('script');
	wp_register_script( 'ajax', get_template_directory_uri() . '/js/ajax.js', array('jquery'),false,true );
	wp_enqueue_script( 'ajax' );
	// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
	wp_localize_script( 'ajax', 'ajax_obj',
	    	array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'smart_nonce' => wp_create_nonce( 'myajax-nonce' )  ) ); 

}
if (!is_admin()) add_action('wp_enqueue_scripts', 'bicweb_style');
// Remove WP Version From Styles
add_filter( 'style_loader_src', 'gda_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'gda_remove_ver_css_js', 9999 );

// Function to remove version numbers
function gda_remove_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
function add_async_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_async = array('bootstrap-script','ajax','mainmenu-style','owl-style','bootstrap-style','main-style','responsive-style','animate-style');

   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', ' async="async" src', $tag);
      }
   }
   return $tag;
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
// Remove Contact Form 7 script and css, add call funtion in contact.php
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

add_action( 'rewrite_rules_array', 'rewrite_rules' );
function rewrite_rules( $rules ) {
    $new_rules = array();
    foreach ( get_post_types() as $t )
        $new_rules[ $t . '/([^/]+).html$' ] = 'index.php?post_type=' . $t . '&name=$matches[1]';
    return $new_rules + $rules;
}

//hihi hehe
// Thêm đuôi .html vào bài chi tiết custome post type
add_filter( 'post_type_link', 'custom_post_permalink' ); // for cpt post_type_link
//add_filter( 'post_link', 'custom_post_permalink' ); //sử dụng lưu trữ post_link
function custom_post_permalink ( $post_link ) {
    global $post;
    $type = get_post_type( $post->ID );
    return home_url( $type . '/' . $post->post_name . '.html' );
}

/*
 * Replace Taxonomy slug with Post Type slug in url
 * Version: 1.1
 */
function taxonomy_slug_rewrite($wp_rewrite) {
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    // get all custom post types
    $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');

    foreach ($post_types as $post_type) {
        foreach ($taxonomies as $taxonomy) {

            // go through all post types which this taxonomy is assigned to
            foreach ($taxonomy->object_type as $object_type) {

                // check if taxonomy is registered for this custom type
                if ($object_type == $post_type->rewrite['slug']) {

                    // get category objects
                    $terms = get_categories(array('type' => $object_type, 'taxonomy' => $taxonomy->name, 'hide_empty' => 0));

                    // make rules
                    foreach ($terms as $term) {
                        $rules[$object_type . '/' . $term->slug . '/?$'] = 'index.php?' . $term->taxonomy . '=' . $term->slug;
                    }
                }
            }
        }
    }
    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
function gda_custom_posttype_query($posttype, $taxonomy, $termId, $numPost){
	$qr =  new WP_Query( array(
			            'post_type' => $posttype,
			            'tax_query' => array(
			                                array(
			                                        'taxonomy' => $taxonomy,
			                                        'field' => 'id',
			                                        'terms' => $termId,
			                                        'operator'=> 'IN'
			                                 )),
			            'showposts'=>$numPost,
			            'order' => 'DESC',
			            'orderby' => 'date'
			     ) );
	return $qr;
}
function gda_custom_posttype_query_paged($posttype, $taxonomy, $termId, $paged, $size){
	$qr =  new WP_Query( array(
			            'post_type' => $posttype,
			            'tax_query' => array(
			                                array(
			                                        'taxonomy' => $taxonomy,
			                                        'field' => 'id',
			                                        'terms' => $termId,
			                                        'operator'=> 'IN'
			                                 )),
						'posts_per_page' => $size,
						'paged' => $paged,
						'order' => 'DESC',
						'orderby' => 'date'
			     ) );
	return $qr;
}
function gda_cus_post_archive_query_paged($posttype, $size){
		$qr =  new WP_Query( array(
			            'post_type' => $posttype,
						'posts_per_page' => $size,
						'order' => 'DESC',
						'orderby' => 'date'
			     ) );
	return $qr;
}

add_action('wp_ajax_BIC_cart_action', 'BIC_cart_action');
add_action('wp_ajax_nopriv_BIC_cart_action', 'BIC_cart_action');
function BIC_cart_action() {
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
    	die ( 'Invalid Request!');

	$data = array();
    if(isset($_POST['act']) && !empty($_POST['act'])){
	    $cart = new BIC_Cart;
        global $post;
        $id = isset($_POST['pid']) ? $_POST['pid'] : 0;
        //Thêm vào giỏ hàng
        if($_POST['act'] === 'addToCart'){
            $post = get_post($id);
            $link = get_post_permalink($id);
            $oldprice = types_render_field( "gia-cu", array("output"=>"raw", "id"=>$id) );
            $price = types_render_field( "gia-moi", array("output"=>"raw", "id"=>$id) );
            $current_price = bicweb_get_price_number($oldprice, $price);
            if (has_post_thumbnail( $post->ID ) ){
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "thumbnail" );
            }
            else{
                $image = array('no-thumb.png');
            }
            $itemData = array(
                'id' => $post->ID,
                'name' => $post->post_title,
                'image' => $image[0],
                'price' => $current_price,
                'qty' => 1,
                'url' => $link
            );
            $insertItem =  $cart->insert($itemData);
        	$data['flag'] = $insertItem;
        	$data['msg'] = $insertItem ? __("Thêm thành công.","bicweb") : __("Lỗi không thể thêm.","bicweb");
        }
        //Cập nhật giỏ hàng
        elseif($_POST['act'] == 'updateItem'){
            $itemData = array(
                'rowid' => $id,
                'qty' => $_POST['qty']
            );
            $updateItem = $cart->update($itemData);
            $data['flag'] = $updateItem;
            $data['msg'] = $updateItem ? __('Cập nhật thành công!','bicweb') : __('Lỗi, cập nhật','bicweb');
        }
        //Xóa sản phẩm
        elseif ($_POST['act'] == 'removeItem') {
            $deleteItem = $cart->remove($id);
            $data['flag'] = $deleteItem;
            $data['msg'] = $deleteItem ? __('Xóa thành công!','bicweb') : __('Lỗi, không thể xóa','bicweb');
        }
    }
    else{
        $data['flag'] = false;
        $data['msg'] = 'No Action';
    }
    // $data['total'] = $cart->total();
    // $data['count'] = $cart->total_items();
    echo json_encode($data);
    die();
}

add_action('wp_ajax_BIC_cart_confirm', 'BIC_cart_confirm');
add_action('wp_ajax_nopriv_BIC_cart_confirm', 'BIC_cart_confirm');
function BIC_cart_confirm() {
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
    	die ( 'Invalid Request!');

	$data = array();
    if(isset($_POST["email"])){	
    	$cart = new BIC_Cart;

		$cus_name = $_POST["name"];
		$cus_email = $_POST["email"];
		$cus_phone = $_POST["phone"];
		$cus_add = $_POST["address"];
		$cus_note = $_POST["messege"];

		//Admin mail
		$adminMail = get_bloginfo('admin_email');
		define( "RECIPIENT_NAME", get_option("blogname") );
		define( "RECIPIENT_EMAIL", $adminMail );
		$to = RECIPIENT_NAME . " <" . RECIPIENT_EMAIL . ">";
		
		$subject = sprintf( __( "Đặt hàng từ: %s", "bicweb" ), get_option( 'blogname' ));
		$headers = "From: Sales " .get_option( 'blogname' )."<sales@dothophunggia.vn>". "\r\n"; //email nền
		//$headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
		$headers .= "CC: ".$cus_email."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message = '<html><body><div style="width: 90%; margin: 40px auto;"';
		$message .= '<p><h2>Thông tin khách hàng:</h2></p>';
		$message .= '<br /><b>Họ tên</b>: '.$cus_name;
		$message .= '<br /><b>Điện thoại</b>: '.$cus_phone;
		$message .= '<br /><b>Email</b>: '.$cus_email;
		$message .= '<br /><b>Địa chỉ</b>: '.$cus_add;
		$message .= '<br /><b>Ghi chú</b>: '.$cus_note;
		$message .= '<p><h2>Chi tiết đơn hàng:</h2></p>';
		$message .= '<table style="width: 100%; border-collapse: collapse">
	        <thead style="color: #ffffff; text-transform: uppercase; line-height: 3;">
	            <tr>
	            	<th style="border-right: 1px solid #ffffff; background: #931901;">'.__("Ảnh sản phẩm", "bicweb").'</th>
	                <th style="border-right: 1px solid #ffffff; background: #931901;">'.__("Tên sản phẩm", "bicweb").'</th>
	                <th style="border-right: 1px solid #ffffff; background: #931901;">'.__("Giá", "bicweb").'</th>
	                <th style="border-right: 1px solid #ffffff; background: #931901;">'.__("Số lượng", "bicweb").'</th>
	                <th style="border-right: 1px solid #ffffff; background: #931901;">'.__("Thành tiền", "bicweb").'</th>
	                <th>&nbsp;</th>
	            </tr>
	        </thead>
	        <tbody style="line-height: 3; text-indent: 10px">';

	            $cur = 'VNĐ'; 
	            if($cart->total_items() > 0){
	                //get cart items from session
	                $cartItems = $cart->contents();
	                foreach($cartItems as $item){
	            		$message .= '
			            <tr>
			                <td style="border-right: 1px solid #cccccc; border-bottom: 1px solid #cccccc; border-left: 1px solid #cccccc;"><figure style="margin: 0; padding-top: 20px; text-align: center;"><img src="'.$item["image"].'" style="width: 120px; height: 80px; margin-right:10px;"></figure></td>
			                <td style="border-right: 1px solid #cccccc; border-bottom: 1px solid #cccccc; text-align: center; font-weight: bold; line-height:2; width: 300px;">'.$item["name"].'</td>
			                <td style="border-right: 1px solid #cccccc; border-bottom: 1px solid #cccccc; text-align: center;">'.bicweb_format_price($item["price"], $cur).'</td>
			                <td style="border-right: 1px solid #cccccc; border-bottom: 1px solid #cccccc; text-align: center;">'.$item["qty"].'</td>
			                <td style="border-right: 1px solid #cccccc; border-bottom: 1px solid #cccccc; text-align: center;">'.bicweb_format_price($item["subtotal"], $cur).'</td>
			                <td></td>
			            </tr>';
		            } 
		        }

	            else{             	
	            	$message .= '<tr><td colspan="5"><p>'.__("Đang trống !","bicweb").'</p></td>';
	            }
	        $message .= '
	        </tbody>
	        <tfoot style="color: #ffffff; line-height: 2; text-transform: uppercase; text-indent: 10px">
	            <tr>
	                <td></td>';
	                if($cart->total_items() > 0){
	                $message .= '<td></td>
		                <td style="background: #931901; border-right: 1px solid #cccccc; line-height: 3; font-weight: bold; text-align: center;">'.__("Tổng", "bicweb").'</td>
		                <td colspan="2" style="border-right: 1px solid #cccccc; background: #931901; line-height: 3; font-size:20px; text-indent: 0; text-align: center;"><strong>'.bicweb_format_price($cart->total(), $cur).'</strong></td>';                          
		            }
		    $message .= '        
	            </tr>
	        </tfoot>
	    </table>';
	    $message .= '<hr /><p><i>Email này được gửi từ ['.get_option("home").']</i></p>';
	    $message .= "</div></body></html>";
		//Thực hiện gửi mail
		$success = mail($to, $subject, $message, $headers);
		$data['flag'] = $success;
		if($success){
			//Xóa giỏ hàng
			$cart->destroy();
			$data["msg"] = __("<p class='tks'>Cảm ơn bạn đã đặt hàng, chúng tôi sẽ gọi điện xác nhận đơn hàng trong thời gian sớm nhất</p>", "bicweb");
		}
		else{
			$data["msg"] = __("Lỗi, đơn hàng gửi không thành công.", "bicweb");
		}	
    }
    else
    	$data["msg"] = __("Email không hợp lệ!", "bicweb");

    echo json_encode($data);
    die();
}

//doc danh muc
add_action( 'wp_ajax_changedanhmuc', 'changedanhmuc' );
add_action('wp_ajax_nopriv_changedanhmuc', 'changedanhmuc');
function changedanhmuc(){
	$nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
    	die ( 'Invalid Request!');
    
    $iddanhmuc = $_POST['iddanhmuc'];

    $term_id = $iddanhmuc;
	$taxonomy_name = 'san-pham-category';

	$term_childs = get_term_children( $term_id, $taxonomy_name );
	$count = count($term_childs);
	if($count<=0) {
		echo '<span class="empty">'.__("Đang cập nhật...","bicweb").'</span>';
	} else {
		foreach ( $term_childs as $child ) {
		    $term = get_term_by( 'id', $child, $taxonomy_name ); 
			echo '<option class="hover-option" value="'.$term->term_id.'">'.$term->name.'</option>';
		}
	}
    die();
}
//doc tat ca sp
add_action( 'wp_ajax_changedanhmuc2', 'changedanhmuc2' );
add_action('wp_ajax_nopriv_changedanhmuc2', 'changedanhmuc2');
function changedanhmuc2(){
	$nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
    	die ( 'Invalid Request!');

    $iddanhmuc2 = $_POST['iddanhmuc2'];

    if($iddanhmuc2 == 9999) {
    	$queryda2 =  new WP_Query( array(
	            'post_type' => 'san-pham',
				'posts_per_page' => 10000,
				'order' => 'DESC',
				'orderby' => 'date'
	     ) );
        if($queryda2->have_posts()) : while ($queryda2->have_posts() ) : $queryda2->the_post();
        	echo '<option class="hover-option" value="'.get_the_ID().'">'.get_the_title().'</option>';
    	endwhile; wp_reset_query(); else: echo '<option value="">Đang cập nhật !</option>'; endif;
    } else {

    }
    die();
}

//doc sp cua dm con
add_action( 'wp_ajax_changedanhmuccon', 'changedanhmuccon' );
add_action('wp_ajax_nopriv_changedanhmuccon', 'changedanhmuccon');
function changedanhmuccon(){
	$nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
    	die ( 'Invalid Request!');

    $iddanhmuccon = $_POST['iddanhmuccon'];

    $queryda = new WP_Query( array(
            'post_type' => 'san-pham',
            'tax_query' => array(
                                array(
                                        'taxonomy' => 'san-pham-category',
                                        'field' => 'id',
                                        'terms' => $iddanhmuccon,
                                        'operator'=> 'IN'
                                 )),
            'posts_per_page' => 1000,
            'order' => 'DESC',
            'orderby' => 'date'
     ) );
    if($queryda->have_posts()) : while ($queryda->have_posts() ) : $queryda->the_post();
    	echo '<option class="hover-option" value="'.get_the_ID().'">'.get_the_title().'</option>';
	endwhile; wp_reset_query(); else: echo '<option value="">Đang cập nhật !</option>'; endif;
    die();
}

add_action('wp_ajax_searchaddToCart', 'searchaddToCart');
add_action('wp_ajax_nopriv_searchaddToCart', 'searchaddToCart');
function searchaddToCart() {
    $nonce = $_POST['nonce'];
    if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
    	die ( 'Invalid Request!');

    $data = array();
    if(isset($_POST['act']) && !empty($_POST['act'])){
	    $cart = new BIC_Cart;    	
        global $post;
        $id = isset($_POST['sid']) ? $_POST['sid'] : 0;
        //Thêm vào giỏ hàng
        if($_POST['act'] === 'searchaddToCart'){
            $post = get_post($id);
            $link = get_post_permalink($id);
            $oldprice = types_render_field( "gia-cu", array("output"=>"raw", "id"=>$id) );
            $price = types_render_field( "gia-moi", array("output"=>"raw", "id"=>$id) );   
            $current_price = bicweb_get_price_number($oldprice, $price);
            if (has_post_thumbnail( $post->ID ) ){            	
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "thumbnail" );
            }
            else{
                $image = array('no-thumb.png');
            }
            $itemData = array(
                'id' => $post->ID,
                'name' => $post->post_title,
                'image' => $image[0],
                'price' => $current_price,
                'qty' => 1,
				'url' => $link
            );
            $insertItem =  $cart->insert($itemData);
            
            //duyet gio hang
            $cartdata= "";
            if($cart->total_items() > 0){
                //get cart items from session
                $cartItems = $cart->contents();
                foreach($cartItems as $item){

            $str=sprintf('onclick="deleteCart(%s)"',"'".$item["rowid"]."'");

                	$cartdata .= '
                		<tr>
                            <td><figure><img src="'.$item["image"].'"></figure></td>
                            <td><a style="color:#000000;" target="_blank" href="'.$item["url"].'">'.$item["name"].'</a></td>
                            <td>'.bicweb_format_price($item["price"], $cur).'</td>
                            <td style="padding-left: 20px;">'.$item["qty"].'</td>
                            <td>'.bicweb_format_price($item["subtotal"], $cur).'</td>
                            <td>
                            	<a class="btnDelete btn btn-primary" href="javascript:void(0)" data-id="'.$item["rowid"].'" '.$str.'><i class="fa fa-times"></i></a>
                            </td>
                        </tr>';
                }
            } else {
            	$cartdata = '<tr><td style="text-align: center;" colspan="6"><p>Bạn chưa chọn sản phẩm nào!</p></td>'; 
            }
            // var $str=sprintf('<a onclick="getItem('%s')"></a>',$ID);

	        $cartdata2= "";
	        $cartdata2 .='
	        	<tr>
                    <td></td>
                    <td></td>';
                    if($cart->total_items() > 0){
	        $cartdata2 .='
	        			<td></td>
	                    <td style="text-align: left; font-weight: bold;">Tổng</td>
	                    <td colspan="2"><strong>'.bicweb_format_price($cart->total(), $cur).'</strong></td>';
                    }
            $cartdata2 .='</tr>';

            // $str2=sprintf('($cart->total_items() > 0) ? "btn-success" : "disabled" ',"'".$item["rowid"]."'");
            $cartdata3= "";
            $cartdata3 .='<a href="'.get_permalink(7).'" class="btn ';
            	if($cart->total_items() > 0) { 
            		$cartdata3 .='btn-success'; 
            	}else{ 
            		$cartdata3 .='disabled';
            	}
            	$cartdata3 .=' pull-right">Giỏ hàng<i class="fa fa-chevron-right"></i></a>';

        	$data['flag'] = $insertItem;
        	$data['msg'] = $insertItem ? __("Thêm thành công.","bicweb") : __("Lỗi không thể thêm.","bicweb");  
        	$data['data'] = $cartdata;
        	$data['data2'] = $cartdata2;
        	$data['data3'] = $cartdata3;
        }
    }
    else{
        $data['flag'] = false;
        $data['msg'] = 'No Action';
        $data['data'] = 'không có sản phẩm nào'; 
    }
    echo json_encode($data);
    die();
}
// require('func/login.php');
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'san-pham'; // change to your post type
	$taxonomy  = 'san-pham-category'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'san-pham'; // change to your post type
	$taxonomy  = 'san-pham-category'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}
?>