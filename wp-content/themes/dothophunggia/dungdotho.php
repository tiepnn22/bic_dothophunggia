<?php
	/*
	Template Name: Page dựng đồ thơ
    */
?>
<?php get_header(); ?>
<?php $cart = new BIC_Cart; ?>
<section class="build fw">
    <div class="container">
        <nav class="breadcrumbs">
            <?php if ( function_exists( 'yoast_breadcrumb' ) ) {
                yoast_breadcrumb('','');};
            ?>
        </nav>
        <div class="build-content">
            <form method="post">
                <div class="select-product-list fw">
                    <div class="select-title">Loại đồ thờ</div>
                    <select id="danhmuc">
                        <option value="9999">Tất cả sản phẩm</option>
                        <?php
                            $terms = get_terms('san-pham-category', array(
                                'parent'=> 0,
                                'hide_empty' => false
                            ) );
                            foreach($terms as $term){
                                if($term->term_id == 20) {

                                } else {
                                    echo "<option class='hover-option' value='".$term->term_id."'>".$term->name."</option>";
                                }
                            }
                        ?>
                    </select>
                </div>

                <div class="select-product-group fw">
                    <div class="select-title">Chọn nhóm sản phẩm</div>
                    <select id="danhmuccon">
                        <option class='hover-option' value=''>Chọn nhóm sản phẩm</option>
                    </select>
                </div>

                <div id="content" class="fw">
                    <div class="select-title">Chọn sản phẩm</div>
                    <select style="width:300px" id="states">
                        <option value="">Search sản phẩm</option>
                        <?php
                            $queryda = gda_cus_post_archive_query_paged('san-pham', 100);
                            if($queryda->have_posts()) : while ($queryda->have_posts() ) : $queryda->the_post(); ?>
                                <option value="<?php echo $post->ID; ?>"><?php the_title(); ?></option>
                        <?php  endwhile; wp_reset_query(); endif; ?>
                    </select>
                    <a class="search-add-to-cart btn btn-primary" data-id="" href="javascript:void(0)">
                        <span>Chọn</span>
                    </a>
                </div>

            </form>
            <div class="select-success fw">
                <div class="select-title-tick">Danh sách sản phẩm đã chọn</div>
                <div class="cart-product-list fw">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Ảnh sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Thành tiền</th>
                                <th style="padding-left:37px;">Xoá</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cur = ' VNĐ';
                            if($cart->total_items() > 0){
                                //get cart items from session
                                $cartItems = $cart->contents();
                                foreach($cartItems as $item){
                            ?>
                            <tr>
                                <td><figure><img src="<?php echo $item["image"]; ?>"></figure></td>
                                <td><a style="color:#000000;" href="<?php echo $item["url"]?>"><?php echo $item["name"]; ?></a></td>
                                <td><?php echo bicweb_format_price($item["price"], $cur); ?></td>
                                <td style="padding-left: 20px;"><?php echo $item["qty"]; ?></td>
                                <td><?php echo bicweb_format_price($item["subtotal"], $cur); ?></td>
                                <td>
                                    <a class="btnDelete btn btn-primary" href="javascript:void(0)" data-id="<?php echo $item["rowid"];?>"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <?php } }else{ ?>
                            <tr><td style="text-align: center;" colspan="6"><p>Bạn chưa chọn sản phẩm nào!</p></td>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <?php if($cart->total_items() > 0){ ?>
                                <td></td>
                                <td style="text-align: left; font-weight: bold;">Tổng</td>
                                <td colspan="2"><strong><?php echo bicweb_format_price($cart->total(), $cur); ?></strong></td>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="link-continue">
                    <!-- <a href="/" class="btn btn-warning"><i class="fa fa-chevron-left"></i>Tiếp tục mua hàng</a> -->
                    <?php
                        $cart = new BIC_Cart;
                    ?>
                    <div class="link-continue-content">
                    <a href="<?php echo get_permalink(7); ?>" class="btn <?php echo ($cart->total_items() > 0) ? 'btn-success' : 'disabled' ?> pull-right">Giỏ hàng<i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ajaxLoad"></div>
</section>
<script type="text/javascript">
    //doc danh muc
    $("#danhmuc").on('change', function() {
        $.ajax({
            type: 'POST',dataType: 'text',
            url: ajax_obj.ajax_url,
            data: {
                'action': 'changedanhmuc',
                iddanhmuc : $("#danhmuc").val(),
                nonce : ajax_obj.smart_nonce
            },
            success: function(response){
                $('#danhmuccon').empty();
                $(document).find('#danhmuccon').append(response);
            }
        });
    });
    //doc tat ca sp
    $("#danhmuc").on('change', function() {
        $.ajax({
            type: 'POST',dataType: 'text',
            url: ajax_obj.ajax_url,
            data: {
                'action': 'changedanhmuc2',
                iddanhmuc2 : $("#danhmuc").val(),
                nonce : ajax_obj.smart_nonce
            },
            success: function(response){
                $('#states').empty();
                $(document).find('#states').append(response);
            }
        });
    });



    //doc sp cua dm con
    $("#danhmuccon").on('change', function() {
        $.ajax({
            type: 'POST',dataType: 'text',
            url: ajax_obj.ajax_url,
            data: {
                'action': 'changedanhmuccon',
                iddanhmuccon : $("#danhmuccon").val(),
                nonce : ajax_obj.smart_nonce
            },
            success: function(response){
                $('#states').empty();
                $(document).find('#states').append(response);
            }
        });
    });

    $('.search-add-to-cart').on('click', function(){
        var id = $("#states").val();
        if(id != 0 || id != ""){
            jQuery.ajax({
                type: "POST",
                data: {
                    'action': 'searchaddToCart',
                    'act' : 'searchaddToCart',
                    'sid' : id,
                    nonce : ajax_obj.smart_nonce
                },
                url: ajax_obj.ajax_url,
                beforeSend:function(){
                    $('.ajaxLoad').show(200);
                },
                success:function(response){
                    data = jQuery.parseJSON(response);
                    $("tbody").html(data.data);
                    $("tfoot").html(data.data2);
                    $(".link-continue-content").html(data.data3);
                    $('.ajaxLoad').hide();
                }
            });
        }
    });

    $(document).ready(function() {
        $("#states").select2();
    });

    $('.btnDelete').on('click', function(){
        var itemID = $(this).data("id");
           deleteCart(itemID);
    });
</script>
<?php get_footer(); ?>