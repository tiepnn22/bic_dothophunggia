<?php get_header(); ?>
<section class="news-detail fw">
	<div class="container">
		<div class="news-detail-content fw">
			<nav class="breadcrumbs">
				<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb('','');};
				?>
			</nav>


<?php if(have_posts()) : while (have_posts() ) : the_post(); ?>
	<div class="news-detail-info">
		<div class="n-title"><h1><a><?php the_title();?></a></h1></div>
		<div class="share">
			<div class="news-detail-date">Ngày đăng: <?php echo get_the_date('Y-m-d');?></div>
			<?php setPostViews(get_the_ID()); ?>
			<div class="news-detail-view">Lượt xem: <?php echo getPostViews(get_the_ID()); ?></div>
			<?php get_template_part("inc/social-bar"); ?>
		</div>
		<?php the_content();?>
	</div>
<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>


			<?php get_template_part("inc/related-post"); ?>
		</div>
		<?php get_sidebar();?>
	</div>
</section>
<?php get_footer(); ?>