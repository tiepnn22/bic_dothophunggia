<?php get_header(); ?>
<section class="search-page fw">
	<div class="container">
		<nav class="breadcrumbs">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb('','');};
			?>
		</nav>
		<div>


<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<article>
		<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('p-detail') ?>" alt="<?php the_title();?>" /></a></figure>
		<div class="n-title"><h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3></div>
		<div class="n-desc"><?php echo cut_string(get_the_excerpt(),150,'...'); ?></div>
	</article>
<?php endwhile; else: echo '<div class="update-loading">Không có kết quả!</div>'; endif; wp_reset_query(); ?>


			<nav class="navigation">
				<?php wp_pagenavi(); ?>
			</nav>
			<div class="clear20"></div>
		</div>
	</div>
</section>
<?php get_footer(); ?>