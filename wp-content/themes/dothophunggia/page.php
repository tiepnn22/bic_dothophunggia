<?php get_header(); ?>
<section class="page-content fw">
	<div class="container">
		<nav class="breadcrumbs">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb('','');};
			?>
		</nav>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="article-content">
				<article class="detail">
					<!-- <h1><?php the_title();?></h1>
					<div class="clear30"></div> -->
					<div class="description">
						<?php the_content();?>
					</div>
				</article>
			</div>
		<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>
	</div>
</section>
<?php get_footer(); ?>