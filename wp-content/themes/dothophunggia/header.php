<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,600,700" rel="stylesheet">
	<?php wp_head(); ?>

<meta name="google-site-verification" content="hS0x-4Tws4zJmMqB3rFyK-IoZrs_DalFqX946Lf0m-c" />

</head>
<body <?php body_class(); ?>>

<header class="fw">
    <div class="header">
		<div class="logo">
			<a href="<?php echo get_option('home');?>"><img src="<?php bloginfo('template_url');?>/images/img/logo.jpg"></a>
		</div>
    	<div class="header-top fw">
	    	<div class="container">
		    	<div class="header-info">
			    	<div class="fix-header-mobile">
			    		<div class="search-box">
		                    <form action="<?php echo esc_url( home_url( '/' ) ); ?>">
		                        <input type="text" placeholder="Tìm kiếm..." name="s" value="<?php echo get_search_query(); ?>">
		                        <button type="submit" class="search-icon"></button>
		                    </form>
			            </div>
			            <a class="cskh" href="tel:19006189">
			            	<span class="cskh-phone"><span>Tổng đài : </span><span>1900 6189</span></span>
			            	<span class="cskh-time">(Từ 8:00 đến 20:30 hàng ngày)</span>
			            	<span class="cskh-time2">(8:00 - 20:30)</span>
			            </a>
			        </div>
			        <a class="fix-search-mobile"></a>
		            <!-- <?php //include("inc/frm-login.php");?> -->
                    <a class="login" target="_blank" href="https://gomsuphunggia.kiotviet.vn"><span class="cskh-phone"><span>Đăng nhập </span></span><span class="cskh-time">(Dành cho cửa hàng, đại lý)</span>
                    	<?php 
	                        // if ( is_user_logged_in() ) {
	                        //     global $current_user;
	                        //     echo '<span class="">Chào '.$current_user->display_name.'</span>';
	                        //     echo '<a class="" href="javascript:void(0)"><i>&nbsp;(thoát)</i></a>';
	                        // }
	                        // else{
	                        //     echo '<a class="" href="javascript:void(0)" data-toggle="modal" data-target="#loginModal">Đăng nhập Đăng ký</a>';
	                        // }
	                    ?>
                    </a>
		            <?php $cart = new BIC_Cart; ?>
					<a class="shopcart" href="<?php echo get_permalink(7); ?>"><span><?php echo 'Giỏ hàng có<span>'.$cart->total_items().' Sản phẩm</span>'; ?></span><span class="shopcart-mobile"><?php echo $cart->total_items(); ?></span></a>
		        </div>
	    	</div>
    	</div>
	    <nav class="menubar fw">
		    <div class="container">
				<div class="main-menu">
					<?php
		                if(function_exists('wp_nav_menu')){
		                    $args = array(
		                        'theme_location' => 'primary',
		                        'link_before'=>'',
		                        'link_after'=>'',
		                        'container_class'=>'',
		                        'menu_class'=>'menu',
		                        'menu_id'=>'',
		                        'container'=>'ul',
		                        'before'=>'',
		                        'after'=>''
		                    );
		                    wp_nav_menu( $args );
		                }
		            ?>
				</div>
			</div>
			<div class="mobile-menu"></div>
		</nav>
	</div>
</header>

<?php if(is_home() || is_page('lien-he')) { } else {?>
	<section class="banner2 fw">
		<div class="content-banner">
			<?php
			    $query = new WP_Query( array(
			            'post_type' => array( 'banner-phung-gia' ),
			            'post_status' => array( 'publish' ),
			            'showposts' => 100,
			            'order' => 'DESC',
			            'orderby' => 'date'
			    ) ); 
			    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>
					<div class="banner-img">
						<img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('full') ?>" />
						<div class="banner-content">
							<div class="banner-title"><a href="<?php echo types_render_field( "url-banner", array("output"=>"raw") ); ?>"><?php echo types_render_field( "text-banner", array("output"=>"raw") ); ?></a></div>
							<div class="banner-desc"><?php the_excerpt(); ?></div>
						</div>
				    </div>
			<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>
	    </div>
	</section>
<?php } ?>