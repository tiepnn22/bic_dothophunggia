<?php get_header(); ?>
	<section class="page-content">
		<div class="container">
			<style type="text/css">
			    .page-404 {
			        min-width: 50%;
			        padding: 0;
			        margin: auto;
			        text-align: center;
			    }
			    .page-404-text-err {
			        font-size: 80px;
			        text-align: center;
			        font-weight: bold;
			        display:block;
			    }
			    .page-404-title{
			    	font-size: 2em;
			    }
			    .page-404-title2 {
				    display: block;
				    margin-bottom: 40px;
				}
			</style>
			<div class="mn-title fw">
				<nav class="breadcrumbs">
    				<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
    					yoast_breadcrumb('','');};
    				?>
    			</nav>
			</div>
			<div class="page-404">
				<img alt="<?php echo get_bloginfo("name"); ?>" src="<?php bloginfo('template_url');?>/images/img/favicon.ico" />
		        <span class="page-404-text-err">404</span>
		        <h1 class="page-404-title">Chúng tôi xin lỗi...</h1>
		        <p>Các trang hoặc thông tin mà bạn đang tìm không thể được tìm thấy.</p>
		        <p><a class="page-404-title2" href="/">Quay trở lại trang chủ</a></p>
			</div>
		</div>
	</section>
<?php get_footer(); ?>