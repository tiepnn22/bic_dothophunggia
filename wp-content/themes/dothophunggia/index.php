<?php get_header(); ?>
<div class="banner-home">
	<?php
		$query = new WP_Query( array(
		        'post_type' => array( 'banner-trang-chu' ),
		        'post_status' => array( 'publish' ),
		        'showposts' => 100,
		        'order' => 'DESC',
		        'orderby' => 'date'
		) );
		if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post(); ?>
			<section class="banner fw">
				<div class="banner-img">
					<img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('full') ?>" />
					<div class="banner-content">
						<div class="banner-title"><a><?php echo types_render_field( "text-banner", array("output"=>"raw") ); ?></a></div>
						<div class="banner-desc"><?php the_excerpt(); ?></div>
						<a href="<?php echo types_render_field( "url-banner", array("output"=>"raw") ); ?>" class="banner-plush">Xem thêm</a>
					</div>
			    </div>
			</section>
	<?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>
</div>

<section class="news-highlight fw">
	<div class="container">
		<div class="news-highlight-content">
			<div class="title"><h2><a>Tin tức nổi bật</a></h2></div>
			<div class="n-items fw">
				<div class="n-group">


<?php $query = new WP_Query(array('cat'=>6,'showposts'=>100,'order' => 'DESC','orderby' => 'date')); $i=0;
if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
if($i!=0&&$i%2==0) echo '</div><div class="n-group">'; ?>
	<article>
		<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('p-thumb') ?>" alt="<?php the_title();?>" /></a></figure>
		<div class="n-title"><h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3></div>
	</article>
<?php $i++; endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>


				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>