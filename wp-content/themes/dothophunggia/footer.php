<section class="country">
	<section class="system fw">
		<div class="container">
			<div class="system-title"><a>Hệ thống phân phối</a></div>
			<div class="domain">Miền bắc</div>
			<div class="system-address">
				<div class="s-title">1. Trụ sở và kho Bát Tràng</div>
				<div class="s-desc">
					<span>Địa chỉ xóm 2, thôn Bát Tràng, xã Bát Tràng, huyện Gia Lâm, TP Hà Nội</span>
					<span>Hotline: <a href="tel:19006189">1900 6189</a></span>
				</div>
				<div class="s-map"><a class="venobox" data-gall="map-gallery" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7450.688696368325!2d105.90960649542095!3d20.978829461503043!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aedc021a0d37%3A0x1dd8abe4d9a9e851!2zQ8O0bmcgVHkgQ-G7lSBQaOG6p24gU-G7qSBCw6F0IFRyw6BuZw!5e0!3m2!1svi!2s!4v1481527266250" data-type="iframe">Bản đồ đường đi</a></div>
			</div>
			<div class="system-address2">
				<div class="s-title">2. Gốm Sứ Phùng Gia tại Cầu Giấy</div>
				<div class="s-desc">
					<span>Địa chỉ số 34, đường Đặng Thùy Trâm, Q.Cầu Giấy, TP Hà Nội</span>
					<span>Hotline: <a href="tel:19006189">1900 6189</a></span>
				</div>
				<div class="s-map"><a class="venobox" data-gall="map-gallery2" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.6760410485404!2d105.78210425036934!3d21.045644592509106!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab33a63d6e33%3A0xabb34976f884cca2!2zxJDhurduZyBUaMO5eSBUcsOibSwgTmdoxKlhIFTDom4sIEPhuqd1IEdp4bqleSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1481527928834" data-type="iframe">Bản đồ đường đi</a></div>
			</div>
		</div>
	</section>

	<footer class="">
		<div class="container">
			<div class="footer-left">
				<div class="footer-top fw">
					<div class="footer-info">
						<div class="f-title">Công ty TNHH gốm sứ Phùng Gia</div>
						<div class="f-desc">
						<?php $social = get_option("wpseo_social"); ?>
							<span>Trụ sở: Xóm 2, Thôn Bát Tràng, Xã Bát Tràng, H Gia Lâm, TP Hà Nội</span>
							<span>Hotline: <a href="tel:19006189">1900 6189</a></span>
							<span>Website: <a target="_blank" href="http://gomsuphunggia.vn">http://gomsuphunggia.vn</a> / <a target="_blank" href="<?php echo get_option('home');?>"><?php echo get_option('home');?></a></span>
							<span>Email: <a href="mailto:<?php echo $social['google_plus_url']; ?>"><?php echo $social['google_plus_url']; ?></a></span>
						</div>
					</div>
					<div class="footer-menu">
						<div>Chính sách chung</div>
					    <?php
				            if(function_exists('wp_nav_menu')){
				                $args = array(
				                    'theme_location' => 'secondary',
				                    'link_before'=>'',
				                    'link_after'=>'',
				                    'container_class'=>'',
				                    'menu_class'=>'policy',
				                    'menu_id'=>'',
				                    'container'=>'ul',
				                    'before'=>'',
				                    'after'=>''
				                );
				                wp_nav_menu( $args );
				            }
				        ?>
					</div>
				</div>
				<div class="footer-bottom fw">
					<div class="socical">
					<?php $social = get_option("wpseo_social"); ?>
						<a target="_blank" href="" title=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
						<a target="_blank" href="" title=""><i class="fa fa-rss" aria-hidden="true"></i></a>
						<a target="_blank" href="<?php echo $social['facebook_site']; ?>" title=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a target="_blank" href="<?php echo $social['twitter_site']; ?>" title=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a target="_blank" href="<?php echo $social['google_plus_url']; ?>" title=""><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
					</div>
					<div class="copyright">
						<span class="fix-footer">© COPYRIGHT BY <a target="_blank" href="http://dothophunggia.vn">DOTHOPHUNGGIA.VN</a></span> - WEB DESIGNED BY <a target="_blank" href="http://bicweb.vn">BICWEB.VN</a>
					</div>
				</div>
			</div>
			<div class="fanpage">
				<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgomsuphunggia.vn%2F%3Ffref%3Dts&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			</div>
		</div>
	</footer>
</section>

<?php if(wp_is_mobile()){ ?>
	<div class='popup-content'>
        <a class="pop-cskh" href="tel:19006189">1900 6189</a>
        <a class="pop-login" href="https://gomsuphunggia.kiotviet.vn">Đăng nhập</a>
		<div class="pop-search-box">	
            <form action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <input type="text" placeholder="Tìm kiếm..." name="s" value="<?php echo get_search_query(); ?>">
                <button type="submit" class="search-icon"></button>
            </form>
        </div>
	</div>
	<div class='popup-plush'><i class='fa fa-plus' aria-hidden='true'></i></div>
<?php } ?>

<div id="back-to-top">
	<i class="fa fa-arrow-up" aria-hidden="true"></i>
</div>

<?php wp_footer(); ?>
</body>
</html>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/596b05141dc79b329518e865/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->



