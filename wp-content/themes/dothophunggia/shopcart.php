<?php
    /*
    Template Name: Trang giỏ hàng
    */
?>
<?php get_header(); ?>
<?php $cart = new BIC_Cart; ?>
<section class="cart fw">
    <div class="container">
        <div class="">
            <div class="cart-content">
                <nav class="breadcrumbs">
                    <?php if ( function_exists( 'yoast_breadcrumb' ) ) {
                        yoast_breadcrumb('','');};
                    ?>
                </nav>
                <div class="cart-detail fw">
                    <div class="cart-product-list fw">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Ảnh sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Thành tiền</th>
                                <th style="padding-left:37px;">Xoá</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cur = ' VNĐ';
                            if($cart->total_items() > 0){
                                //get cart items from session
                                $cartItems = $cart->contents();
                                foreach($cartItems as $item){
                            ?>
                            <tr>
                                <td><figure><img src="<?php echo $item["image"]; ?>"></figure></td>
                                <td><a style="color:#000000;" href="<?php echo $item["url"]?>"><?php echo $item["name"]; ?></a></td>
                                <td><?php echo bicweb_format_price($item["price"], $cur); ?></td>
                                <td><input type="number" class="input-num text-center" value="<?php echo $item["qty"]; ?>"><a class="btnUpdate btn btn-primary" href="javascript:void(0)" data-id="<?php echo $item["rowid"];?>" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
                                <td><?php echo bicweb_format_price($item["subtotal"], $cur); ?></td>
                                <td>
                                    <a class="btnDelete btn btn-primary" href="javascript:void(0)" data-id="<?php echo $item["rowid"];?>"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <?php } }else{ ?>
                            <tr><td colspan="6"><p>Bạn chưa chọn sản phẩm nào!</p></td>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <?php if($cart->total_items() > 0){ ?>
                                <td></td>
                                <td style="text-align: left; font-weight: bold;">Tổng</td>
                                <td colspan="2"><strong><?php echo bicweb_format_price($cart->total(), $cur); ?></strong></td>                                
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                    <div class="link-continue">
                        <!-- <a href="/" class="btn btn-warning"><i class="fa fa-chevron-left"></i>Tiếp tục mua hàng</a> -->
                        <?php 
                            $cart = new BIC_Cart;
                        ?>
                        <a href="<?php echo get_permalink(14); ?>" class="btn <?php echo ($cart->total_items() > 0) ? 'btn-success' : 'disabled' ?> pull-right">Đặt hàng<i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var itemID;
    $('.btnDelete').on('click', function(){
        itemID = $(this).data("id");
        // if(confirm("<?php _e('Bạn có chắc không ?', 'bicweb'); ?>")){
           deleteCart(itemID);
        // }
    });
    $('.btnUpdate').on('click', function(){
        itemID = $(this).data("id");
        var item = $(this).parent().parent();
        var qty = item.find('input[type=number]').val();       
        updateCart(itemID,qty);
    });
</script>
<?php get_footer(); ?>