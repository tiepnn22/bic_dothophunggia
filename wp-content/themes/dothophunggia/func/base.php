<?php

/**
	Hộp thoại xuất hiện theme support trong admin
**/
// add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

// function my_custom_dashboard_widgets() {
// global $wp_meta_boxes;

// wp_add_dashboard_widget('custom_help_widget', 'Theme Support', 'custom_dashboard_help');
// }

// function custom_dashboard_help() {
// 	echo '<p>Chào mừng bạn đến với hệ quản trị website. Hỗ trợ website <a href="tel:+8436401821">tại đây</a>. Tư vấn thiết kế website: <a href="http://bicweb.vn" target="_blank">BicWeb</a></p>';
// }

/**
	Xóa footer trong admin
**/
function remove_footer_admin () {
	echo 'Thiết kế website bởi <a href="http://bicweb.vn" target="_blank">BicWeb</a> | Mã nguồn website <a href="http://www.wordpress.org" target="_blank">WordPress</a></p>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

/**
	Add a Custom Dashboard Logo
**/
function wpb_custom_logo() {
	echo '
		<style type="text/css">
			#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon::before {
			    background-image: url('.get_template_directory_uri().'/images/logo.png);
			    background-position: center center;
			    background-repeat: no-repeat;
			    background-size: contain;
			    -moz-background-size: contain;
				-webkit-background-size: contain;
				-o-background-size: contain;
				-ms-background-size: contain;
			    content: "";
			    display: inline-block;
			    height: 20px;
			    left: 3px;
			    top: 3px;
			    width: 25px;
			}
		</style>';
}
add_action('wp_before_admin_bar_render', 'wpb_custom_logo');

/**
	Add a Custom Login Logo
**/
function my_login_logo_one() {
	echo '
		<style type="text/css">
			body.login div#login h1 a {
				background-image: url('.get_template_directory_uri().'/images/logo.png);
			    background-position: center center;
			    background-repeat: no-repeat;
			    background-size: contain;
			    -moz-background-size: contain;
				-webkit-background-size: contain;
				-o-background-size: contain;
				-ms-background-size: contain;
			    display: inline-block;
			    height: 200px;
			    line-height: 0;
			    margin: -30px auto 0;
			    width: 200px;
			}
		</style>';
}
add_action( 'login_enqueue_scripts', 'my_login_logo_one' );