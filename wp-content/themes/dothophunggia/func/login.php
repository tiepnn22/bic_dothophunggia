<?php
	/**
	 * This Ajax function will register the new user
	 * @param $_POST['email']
	 * @param $_POST['username']
	 */
	add_action('wp_ajax_uregister', 'uregister');
	add_action('wp_ajax_nopriv_uregister', 'uregister');
	function uregister() {
		$nonce = $_POST['nonce'];
		if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
		die ( 'Invalid Request!');
		
		$data = array();
		$user_email = $_POST['email'];
		$user_name = $_POST['username'];
		$user_id = username_exists( $user_name );
		
		if ( $user_id) $user_name = ieb_wpUniqueUser($email);
		
		if ( !$user_id and email_exists($user_email) == false ) {
			$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			$user_id = wp_create_user( $user_name, $random_password, $user_email );
			//SendWelcomeEmail( $user_id, $random_password );
			$data['flag'] = true;
			$data['msg'] = "Your account is created, Please login to your email <strong>$user_email</strong> for login details.<br/><br/> <a href='javascript:;' class='signin-tab'>Click here to login</a>";
		} else {
			$data['flag'] = false;
			$data['msg'] = "Email already exists. Please <a href='javascript:;' class='signin-tab'>login</a> using $user_email";
		}
		echo json_encode($data);
		die();
	}

	/**
	 * This Ajax function will reset user password and mail to the user
	 * @param $_POST['email']
	 */

	add_action('wp_ajax_ureset_password', 'ureset_password');
	add_action('wp_ajax_nopriv_ureset_password', 'ureset_password');
	function ureset_password() {
		
		$nonce = $_POST['nonce'];
		if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
		die ( 'Invalid Request!');
		
		$data = array();
		$user_email = $_POST['email'];
		$user = get_user_by( 'email', $user_email );
		$user_id = $user->ID;
		if(!empty($user_id)){
			$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
			wp_set_password( $random_password, $user_id );
			//SendWelcomeEmail( $user_id, $random_password );
			$data['flag'] = false;
			$data['msg'] = "We sent you an email with reset instructions!!";
		}else{
			$data['flag'] = false;
			$data['msg'] = "Email not exists, Please enter valid email!!";
		}
		echo json_encode($data);
		die();
	}

	/**
	 * This Ajax function will handle user authentication part
	 * @param $_POST['username']
	 * @param $_POST['password']
	 */
	add_action('wp_ajax_login', 'login');
	add_action('wp_ajax_nopriv_login', 'login');

	function login() {
		
		$nonce = $_POST['nonce'];
		if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
		die ( 'Invalid Request!');

		$creds = array();
		$data = array();
		$creds['user_login'] = $_POST['username'];
		$creds['user_password'] = $_POST['password'];
		$creds['remember'] = false;
		$user = wp_signon( $creds, false );
		if ( is_wp_error($user) ){
			$data['flag'] = false;
			$data['msg'] = $user->get_error_message();
		}else{
			$data['flag'] = true;
			$data['msg'] = "Login Successful. Please wait...";
		}
		echo json_encode($data);
		die();
	}

	/**
	 * This Ajax function will check unique username
	 * @param $_POST['username']
	 */
	add_action('wp_ajax_user_exists', 'user_exists');
	add_action('wp_ajax_nopriv_user_exists', 'user_exists');
	function user_exists() {

		$nonce = $_POST['nonce'];
		if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
		die ( 'Invalid Request!');


		$username = $_POST['username'];
		$data = array();
		if ( username_exists( $username ) ){
			$data['flag'] = false;
			$data['msg'] = "Username In Use!";
		}else{
			$data['flag'] = true;
			$data['msg'] = "Username Not In Use!";
		}
		echo json_encode($data);
		die();
	}
	/**
	 * This Ajax function will check unique email
	 * @param $_POST['email']
	 */
	add_action('wp_ajax_email_exists', 'uemail_exists');
	add_action('wp_ajax_nopriv_email_exists', 'uemail_exists');
	function uemail_exists() {
		$nonce = $_POST['nonce'];
		if ( ! wp_verify_nonce( $nonce, 'myajax-nonce' ) )
		die ( 'Invalid Request!');

		$email = $_POST['email'];
		$data = array();
		if ( email_exists( $email ) ){
			$data['flag'] = false;
			$data['msg'] = "Email already exists!";
		}else{
			$data['flag'] = true;
			$data['msg'] = "Email Not In Use!";
		}
		echo json_encode($data);
		die();
	}

	/* the php log out ajax action */
	function ajax_logout(){

	        // First check the nonce, if it fails the function will break
	        wp_verify_nonce( $_POST['security'], 'log-out' );

	        wp_logout();

	        ob_start();
	        gzp_not_logged_in_menu();
	        $menu_content = ob_get_contents();
	        ob_end_clean();

	        echo json_encode(array('loggedout'=>true, 'message'=>__('Logout successful, redirecting...'), 'menu_content'=>$menu_content));

	        die();
	}
	add_action( 'wp_ajax_ajaxlogout', 'ajax_logout' );
?>