<?php get_header(); ?>
<section class="news-list fw">
	<div class="container">
		<nav class="breadcrumbs">
			<?php if ( function_exists( 'yoast_breadcrumb' ) ) {
				yoast_breadcrumb('','');};
			?>
		</nav>
		<div class="news-list-content-slide">
			<div class="news-list-content fw">


<?php $t=0; if(have_posts()) : while(have_posts()) : the_post(); ?>
	<article>
		<div class="n-p">
			<?php if($t == 0) { ?>
					<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('full') ?>" alt="<?php the_title();?>" /></a></figure>
			<?php } else { ?>
					<figure><a href="<?php the_permalink();?>"><img class="img-responsive" src="<?php echo bicweb_get_thumbnail_url('p-thumb') ?>" alt="<?php the_title();?>" /></a></figure>
			<?php } ?>
					<div class="n-title"><h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3></div>
		</div>

			<?php if($t == 0) { ?>
					<div class="n-desc"><?php echo cut_string(get_the_excerpt(),600,'...'); ?></div>
			<?php } ?>
	</article>
<?php $t++; endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>


				<nav class="navigation">
					<?php wp_pagenavi(); ?>
				</nav>
				<div class="clear20"></div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>