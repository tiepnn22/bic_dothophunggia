<?php
	/*
	Template Name: Trang liên hệ
	*/
?>
<?php get_header(); ?>
<?php
    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    }
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
        wpcf7_enqueue_styles();
    }
?>
<div id="map"></div>

<section class="contact fw">
    <div class="container">
        <div class="contact-content">
            <div class="contact-logo"><a href="<?php echo get_option('home');?>"><img src="<?php bloginfo('template_url');?>/images/icon/logo-lienhe.jpg"></a></div>
            <div class="contact-title">Công ty TNHH Gốm sứ Phùng Gia</div>
            <div class="contact-desc">
            <?php $social = get_option("wpseo_social"); ?>
                <span>Trụ sở chinh, kho, xưởng sản xuất: Xóm 2, thôn Bát Tràng, X Bát Tràng, H Gia Lâm, TP Hà Nội</span>
                <span>Hotline: <a href="tel:19006189">1900 6189</a></span>
                <span>Website: <a target="_blank" href="http://gomsuphunggia.vn">http://gomsuphunggia.vn</a> / <a target="_blank" href="<?php echo get_option('home');?>"><?php echo get_option('home');?></a></span>
                <span>Email: <a href="mailto:<?php echo $social['google_plus_url']; ?>"><?php echo $social['google_plus_url']; ?></a></span>
                <span>Fanpage: <a target="_blank" href="https://www.facebook.com/gomsuphunggia.vn"><?php echo $social['facebook_site']; ?></a></span>
            </div>
            <div class="form">
                <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
                        <!-- <?php echo do_shortcode('[contact-form-7 id="462" title="Form Liên Hệ"]'); ?> -->
                        <?php the_content();?>
                <?php endwhile; wp_reset_query(); else: echo '<div class="update-loading">Đang cập nhật!</div>'; endif; ?>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDxXlqTr2NQ6_pS9FWD9aX7NNyWEBzFZ_0&language=vi"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/googlemaps.js"></script>
<script type="text/javascript">
        // Google Map Settings
    if($('#map').length){
        var map;
         map = new GMaps({
            el: '#map',
            zoom: 18,
            scrollwheel:false,
            //Set Latitude and Longitude Here
            lat: 20.976526,
            lng: 105.911247
          });
          
          //Add map Marker
          map.addMarker({
            lat: 20.976526,
            lng: 105.911247,
            infoWindow: {
              content: '<p class="info-outer" style="text-align:center;"><strong>Công ty TNHH Gốm Sứ Phùng Gia</strong><br>Xóm 2, thôn Bát Tràng, xã Bát Tràng, huyện Gia Lâm, TP Hà Nội</p>'
            },
            
            // icon: "favicon.ico"
            icon: '<?php bloginfo('template_url')?>/images/img/logo-contact.png'
        });
    }
</script>
<?php get_footer(); ?>